var host = window.location.host
ws = new WebSocket("ws://" + host.replace(":8080","") + ":8081/ws");

ws.onopen = function(e) {
    echo("连接成功！");
    var data = {}
    data.message = "来！宝贝儿，mysql监控走起！"
    data.type = "mysql"
    messageJson = json(data);
    ws.send(messageJson)   
}
ws.onclose = function(evt) {
    echo("连接关闭");
    ws = null;
}
ws.onmessage = function(e) {
    data = $.parseJSON(e.data);
    if (data.type == "mysql"){
        id("Mysqlbox").innerHTML = data.logs
    }else if(data.type == "ADDDIR"){
        if(data.path.length > 0 && data.status == "yes"){
            let Data = {};
            Data.type = "GetPathInfo";
            Data.data = data.path;
            ws.send(json(Data))
        }else{
            alert("创建失败！" + data.body)
        }
    }else if(data.type == "DELDIR"){
        if(data.status == "no"){
            alert("删除失败")
        }else{
            let Data = {};
            Data.type = "GetPathInfo";
            Data.data = data.path;
            ws.send(json(Data))
        }
    }else if(data.type == "api"){
        $(".apiBody pre").html(data.body)
        let header = "<hr/>"
        if(data.header){
            for(k in data.header){
                header += "<p style='color:#000;padding:2px;font-size:16px;font-weight:normal;'>" + k + "：  " + data.header[k] + "</p>"
            }
        }
        Performance(data.hs,data.url)
        $(".apiBody pre .HaoShi").html($(".apiBody pre .HaoShi").html() + header)
    }else if(data.type == "STRREPLACE"){
        $(".apiBody pre .jieguo").html(data.body + $(".apiBody pre .jieguo").html())
    }else if(data.type == "STRSEARCH" || data.type == "DIRSEARCH"){
        if(data.jindu == "yes" && data.status == "no"){
            $(".apiBody pre .jindu").html(data.path)
        }else if(data.status == "no" && data.jindu == "runing"){
            $(".apiBody pre .progress-span").html(data.progress)
            $(".apiBody pre  progress").val(data.progress)           
        }else if(data.status == "no" && data.jindu == "yes"){
            $(".apiBody pre .jindu").html(data.body)
        }else if(data.status == "yes" && !data.haoshi){
            $(".apiBody pre .jieguo").append(data.body)
        }else{
            $(".apiBody pre .jindu").html(data.body)
        }
    }else if(data.type == "GetFileInfo"){
        let model = $("#debugModel option:selected").val()
        let path = data.path
        //网页 api模式
        //if($.inArray(model,fileSystemModels) != -1){
            if(data.status == "no"){
                alert(data.body)
                $(".fileDir").html("")
                $("#code").attr("readonly","readonly")
                return
            }

            //准备打开列表
            //先判断目录是否已经打开
            if($.inArray(path,fileOpenLists) == -1){
                fileOpenLists.push(path)
            }
            iniFileOpenLists(path)
            //给关闭按钮添加事件
            $(".fileLists >button .close").bind("click",function(event){
                event.stopPropagation();//阻止冒泡
                removeEdit(this)
            })
            $("#code").removeAttr("readonly")
            $("#codeBoxWidthSet").val(parseInt(PageWidth) / 2)
            $("#codeBoxWidthSet").attr("max",parseInt(PageWidth))
            $(".fileDir").html(data.path)
            codeMain = ace.edit("codeMain",{
                wrap: true,
                showInvisibles:false,
                showPrintMargin: false,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true,
                useSoftTabs:false,
                tabSize:4,
                keyboardHandler:'sublime',
                theme:"ace/theme/monokai"
        });
        codeMain.resize();
       // editor.setTheme("ace/theme/monokai");
        codeMain.setValue(data.body)
        Lang(codeMain,data.path)
        $("#codeMain").css({
            width: $(".codeBox").width + "px",
            height: PageHeight - 300 + "px"
        })
        //解决第一次打开文件闪移问题
        if(fileOpenLists.length <= 1 && openEditCounts == 0){
            //在渲染一次
            openEdit()
            openEditCounts++
        }
    }else if(data.type == "GetPathInfo"){
        if(data.status == "no"){
            $(".apiBody pre").html("<h1>" + data.body +"</h1>" )
            return 
        }
        $(".dirLists").html(data.body)
        if (data.path != "disk") {
            $(".nowPath").html(data.path)
        }else{
            $(".nowPath").html("")
        }
        bindDel()
    }else if(data.type == "IPTOCITY" || data.type == "TIMETODATE"){
        $(".apiBody pre").html("<h1>" + data.body +"</h1>" )
    }else if(data.type == "REGXP"){
        $("#regStr").val( data.body)
        updateModelsUrlValues();
    }else if(data.type == "PressureTest"){
        if (data.status == "err"){
            $("#DeBug").css("pointer-events","auto")
            ShowMsg(data.body)
        }else if (data.finish) {
            $(".apiBody pre .jindu").append(data.body)
            var progress = (parseFloat(data.finish) / parseFloat(data.total)  * 100).toFixed(2)
            $(".apiBody pre .JieGuo .progress-span").html(progress)
            $(".apiBody pre .JieGuo progress").val(progress)
            var n = parseInt($(".apiBody").innerHeight()) / 18 ;
            if(parseInt(data.finish) > n){
                var top = (parseInt(data.finish) - n) * 18;
                $(".apiBody pre .jindu").css("top", "-" + top + "px")
                if (parseInt(data.finish) == parseInt(data.total)){
                    $(".apiBody pre .jindu").css("top", "-" + (top + 75) + "px")
                    setTimeout(function(){
                        $(".apiBody pre .jindu").append('<button onclick="readAll()" class="readAll">查看全部</button>')
                    },100)
                }
            }
        }else{
            $(".apiBody pre .JieGuo").append(data.body)
            $("#DeBug").css("pointer-events","auto")
            yaLiPerformance(data.now,data.model,data.url)
        }
    }
}
ws.onerror = function(evt) {
    echo("ERROR: " + evt.data);
}
