//编程语言
let Langs = [
    ["文本","text"],
    ["php","php"],
    ["css","css"],
    ["django","django"],
    ["go","golang"],
    ["js","javascript"],
    ["ts","typescript"],
    ["objectivec","objectivec"],
    ["php_laravel_blade","php_laravel_blade"],
    ["md","markdown"],
    ["markdown","markdown"],
    ["apache_conf","apache_conf"],
    ["powershell","powershell"],
    ["vbscript","vbscript"],
    ["ruby","ruby"],
    ["sass","sass"],
    ["sql","sql"],
    ["swift","swift"],
    ["html","html"],
    ["htm","html"],
    ["tpl","html"],
    ["json","json"],
    ["java","java"],
    ["jsp","jsp"],
    ["lua","lua"],
    ["py","python"],
    ["xml","xml"],
    ["sh","sh"],
    ["nginx","nginx"],
]
let openEditCounts = 0;
let setIntervalId = 0;
let x = 0;
let codeMain = null;
let XId = 0;
let reg = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([\w-~]+).)+([\w-~\/])+/
let ipReg = /[1-9]+(\.\d+)+/  //ipv4 ipv6 
let urlModels  = ["NORMAL","GET","POST","YALI-GET","YALI-POST"]
let modelsUrlValues = {}  //模式输入框寄存器
let apiModels = ["GET","POST"]
let yaliModels = ["YALI-GET","YALI-POST"]
let rightModels = ["YALI-GET","YALI-POST","REGXP","CHEST"]
let fileSystemModels = ["NORMAL","GET","POST"]
let fileSystemModels2 = ["STRSEARCH","DIRSEARCH"]
let CurrentDir = ""  //路径寄存器
let PageWidth = 0
let PageHeight = 0
let fileOpenLists = []
let ChangeChanelCount = 0  //和openmysql  closemysql配置使用的 监听是否初始化过
let dragLists = [
    //{element:"#Mysqlbox",scope:"body"},
    {element:".performance",scope:"body"},
] 
//拖曳列表
let disableList = [
    ".serachP",
    ".performance"
]
//性能耗时对象
let perFormance = {
    isOpen:false,
    NORMAL:{},
    GET:{},
    POST:{},
    YALIGET:{},
    YALIPOST:{}
}
//调试的开始时间
let startTime = 0
//调试的结束时间
let endTime = 0